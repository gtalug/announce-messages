"""
This file is no longer used. Use meeting-announce.py and .gitlab-ci.yml
for current semi-automated job of sending email.
"""

import os
import smtplib
from email.mime.text import MIMEText

from dateutil.rrule import rrule, MONTHLY, TU
from dateutil.parser import parse as dateutil_parse

from fabric.api import task, prompt, local, env
from fabric.utils import abort, fastprint


@task
def new_meeting():
    with open('_templates/meeting.txt.tpl', 'r') as f:
        meeting_template = f.read()

    date_prompt = dateutil_parse(prompt('Date:'))
    title_prompt = prompt('Title:')

    date = list(rrule(MONTHLY, count=1, byweekday=TU, bysetpos=2,
                      dtstart=date_prompt))[0]

    filepath = 'meetings/{0}.txt'.format(date.strftime('%Y-%m'))

    if os.path.isfile(filepath):
        return abort('Meeting already exists.')

    data = {
        'date': date,
        'title': title_prompt,
        'body': '====\nREPLACE\n===='
    }

    with open(filepath, 'w') as f:
        f.write(meeting_template.format(**data))


@task
def send_meeting():
    """
    Send out meeting announce email using interactive or batch mode

    Sample batch command:
        $ fab --set date=2019-05,subject="Round Table and Q&A Session" send_meeting
    """
    date = None
    if 'date' in env and 'subject' in env:
        date = env['date']
        subject = env['subject']
    else:
        date = prompt('Date:', validate=r'(\d{4})-(\d{2})')
        subject = prompt('Subject:')

    email_from = 'hi@gtalug.org'
    email_to = 'announce@gtalug.org'

    filepath = 'meetings/{0}.txt'.format(date)

    # Pull when running in interactive mode
    if 'date' not in env:
        local('git pull')

    if not os.path.isfile(filepath):
        return abort('There is no meeting at that date.')

    with open(filepath, 'r') as f:
        msg = MIMEText(f.read())

    msg['Subject'] = subject
    msg['From'] = email_from
    msg['To'] = email_to

    if 'date' in env and 'subject' in env:
        s = smtplib.SMTP('127.0.0.1')
        s.sendmail(email_from, [email_to], msg.as_string())
        s.quit()
    else:
        preview = "{0}".format(msg.as_string())
        fastprint(preview)
        send = prompt('Everything look okay?', validate=r'(yes|no)')
        if send == 'yes':
            s = smtplib.SMTP('127.0.0.1')
            s.sendmail(email_from, [email_to], msg.as_string())
            s.quit()


@task
def new_event():
    """Create a new GTALUG event based on event.txt.tpl template"""
    with open('_templates/event.txt.tpl', 'r') as f:
        meeting_template = f.read()

    date_prompt = dateutil_parse(prompt('Date:'))
    title_prompt = prompt('Title:')

    date = date_prompt

    filepath = 'events/{0}.txt'.format(date.strftime('%Y-%m-%d'))

    if os.path.isfile(filepath):
        return abort('Event already exists.')

    data = {
        'date': date,
        'title': title_prompt,
        'body': '====\nREPLACE\n===='
    }

    with open(filepath, 'w') as f:
        f.write(meeting_template.format(**data))


@task
def send_event():
    date = prompt('Date:', validate=r'(\d{4})-(\d{2})-(\d{02})')
    subject = prompt('Subject:')
    email_from = 'hi@gtalug.org'
    email_to = 'announce@gtalug.org'

    filepath = 'events/{0}.txt'.format(date)

    local('git pull')

    if not os.path.isfile(filepath):
        return abort('There is no event at that date.')

    with open(filepath, 'r') as f:
        msg = MIMEText(f.read())

    msg['Subject'] = subject
    msg['From'] = email_from
    msg['To'] = email_to

    preview = "{0}".format(msg.as_string())

    fastprint(preview)

    send = prompt('Everything look okay?', validate=r'(yes|no)')

    if send == 'yes':
        s = smtplib.SMTP('127.0.0.1')
        s.sendmail(email_from, [email_to], msg.as_string())
        s.quit()
