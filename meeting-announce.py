#!/usr/bin/env python

# This script is compatible with Python 2.7 and Python 3.x

import os
import sys
import subprocess
import smtplib
import socket
from email.mime.text import MIMEText
from datetime import datetime
from config import Config


from pytz import timezone
from gitlab import Gitlab
from dateutil.rrule import rrule, MONTHLY, TU


config = Config()
eastern = timezone(config.TIMEZONE)
now = eastern.localize(datetime.now())


def get_desc(item):
    return item.split(":")[1].strip("' \"")


def get_meeting_info(meeting_info):
    meeting_body = " ".join(meeting_info[7:])
    meeting_title = get_desc(meeting_info[0])
    meeting_date = ":".join(meeting_info[1].split(":")[1:]).strip("' ")
    meeting_date = datetime.strptime(meeting_date, "%Y-%m-%dT%H:%M:%S")
    meeting_speaker = get_desc(meeting_info[2])
    return (
        meeting_date,
        " with ".join([meeting_title, meeting_speaker]),
        meeting_body)


def get_next_meeting_date():
    """Get the next meeting date, including the one on the same day as today"""
    evening_meeting = (
        datetime.now()
        .replace(hour=19, minute=30, second=0, microsecond=0)
    )
    next_date = list(
        rrule(
            MONTHLY,
            count=2,
            dtstart=evening_meeting,
            byweekday=TU,
            bysetpos=2)
        )
    for date_pos in next_date:
        if date_pos > datetime.now():
            return date_pos


def next_meeting(repo):
    """Get next meeting info from website repo"""
    next_meeting = get_next_meeting_date().date()
    next_meeting_doc = 'meetings/{0}.markdown'.format(next_meeting.strftime('%Y-%m'))
    all_meetings = {
        doc['path']
        for doc in repo.repository_tree(path="meetings", all=True)}
    if next_meeting_doc in all_meetings:
        meeting = repo.files.get(next_meeting_doc, ref='master').decode()
        if sys.version_info > (3, 0):
            meeting = str(meeting, 'utf-8')
        return meeting.split('\n')


def create_meeting(meeting_date, title, description):
    with open('_templates/meeting.txt.tpl', 'r') as f:
        meeting_template = f.read()

    filepath = 'meetings/{0}.txt'.format(meeting_date.strftime('%Y-%m'))

    if os.path.isfile(filepath):
        print('Meeting already exists.')
        return filepath
    data = {
        'date': meeting_date,
        'title': title,
        'body': description
    }

    with open(filepath, 'w') as f:
        f.write(meeting_template.format(**data))
    subprocess.call(["git", "add", filepath])
    subprocess.call([
        "git", "commit", "-m", "Added announcement {}".format(filepath)])
    subprocess.call(["git", "push"])
    return filepath


def mail_subject_interval(meeting_date):
    """
    Given meeting_date, return appropriate email subject depending on time
    until the start of the meeting:
    - Meeting Today
    - Meeting Tomorrow
    - Meeting on {meeting_date}
    """
    now = datetime.now()

    meeting_midnight = meeting_date.replace(hour=0, minute=0)
    # Today (from midnight until the meeting)
    day_len = meeting_date - meeting_midnight
    # two days (from midnight d-1 until midnight d)
    if (meeting_date - day_len) < now:
        return "Meeting Today"
    elif (meeting_date - day_len) > now and (meeting_date - day_len * 2) < now:
        return "Meeting Tomorrow"
    else:
        return "Meeting on {}".format(
            meeting_date.strftime("%A, %B %d at %I:%M %p"))


class AnnounceMsg(object):
    """Code related to assembling and sending announce message"""
    email_from = 'hi@gtalug.org'
    email_to = 'announce@gtalug.org'

    def __init__(self, meeting_date, filepath):
        """Given date and path to meeting description, compose announce email"""
        self.msg = ''

        with open(filepath, 'r') as f:
            self.msg = MIMEText(f.read())

        self. msg['Subject'] = mail_subject_interval(meeting_date)
        self.msg['From'] = self.email_from
        self.msg['To'] = self.email_to

    def preview(self):
        """Print out message preview to stdout"""
        print("Meeting preview")
        print("===============================================")
        print(("{0}".format(self.msg.as_string())))
        print("===============================================")

    def send(self):
        """Given MIMEText message, send the meeting via SMTP localhost"""
        s = smtplib.SMTP('127.0.0.1')
        s.sendmail(self.email_from, [self.email_to], self.msg.as_string())
        s.quit()


if __name__ == '__main__':

    gitlab = Gitlab(config.GITLAB_HOST)
    repo = gitlab.projects.get(config.GITLAB_WEBSITE_REPO)

    doc = next_meeting(repo)
    if doc:
        meeting_date, title, body = get_meeting_info(doc)
        filepath = create_meeting(meeting_date, title, body)
        announce_msg = AnnounceMsg(meeting_date, filepath)
        announce_msg.preview()
        if socket.gethostname() == 'penguin.gtalug.org':
            announce_msg.send()
        else:
            print((
                "Can't send email from hostname {}. ".format(socket.gethostname()) +
                """Run this script on penguin to send this message"""
            ))
            sys.exit(1)
    else:
        print("No Next meeting available")
