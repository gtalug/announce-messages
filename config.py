
class Config(object):
    """Repo config"""
    GITLAB_WEBSITE_REPO = 'gtalug/website'
    GITLAB_HOST = 'https://gitlab.com'
    TIMEZONE = 'US/Eastern'
