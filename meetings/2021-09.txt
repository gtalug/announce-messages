https://gtalug.org/meeting/2021-09/

# Lightning talks and Round Table

This is GTALUG's version of an un-conference, a loosely structured short talks emphasizing the informal exchange of information and ideas between participants, rather than following a conventionally structured GTALUG meetings.  If you already have a topic in mind please send an email to  speakers@gtalug.org to be added to the list of scheduled talks.


We're going to use Zoom for this meeting:

Join Zoom Meeting:
https://us02web.zoom.us/j/83647888809?pwd=RHVwZndZY1dZOHRlcE9ER25QUkkrZz09

Meeting ID: 836 4788 8809

Passcode: 804603

One tap mobile

+16473744685,,83647888809#,,,,*804603# Canada
+16475580588,,83647888809#,,,,*804603# Canada



Dial by your location

+1 647 374 4685 Canada
+1 647 558 0588 Canada
+1 778 907 2071 Canada
+1 204 272 7920 Canada
+1 438 809 7799 Canada
+1 587 328 1099 Canada

Find your local numberhttps://us02web.zoom.us/u/klYT1laDI

## Schedule

* 7:30 pm - Meeting and presentation.

# Code of Conduct

We want a productive happy community that can welcome new ideas, improve every
process every year, and foster collaboration between individuals with differing
needs, interests and skills.

We gain strength from diversity, and actively seek participation from those who
enhance it. This code of conduct exists to ensure that diverse groups
collaborate to mutual advantage and enjoyment. We will challenge prejudice that
could jeopardise the participation of any person in the community.

The Code of Conduct governs how we behave in public or in private whenever the
Linux community will be judged by our actions. We expect it to be honoured by
everyone who represents the community officially or informally, claims
affiliation or participates directly. It applies to activities online or
offline.

We invite anybody to participate. Our community is open.

Please read more about the GTALUG Code of Conduct here:
https://gtalug.org/about/code-of-conduct/

If you have any questions, comments, or concerns about the GTALUG Code of
Conduct please contact the GTALUG Board @ board@gtalug.org
