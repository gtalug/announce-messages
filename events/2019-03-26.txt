# In The Community

# March

## Toronto AWS User Group
AWS Storage Options and their anti-patterns
* Wednesday, 27th, 6:00 PM
* Myplanet, 252 Adelaide St. E
* https://www.meetup.com/Toronto-AWS-Users-United/events/258967292/

## Toronto Perl Mongers
* Thursday, 28th, 7:00 PM
* FlightNetwork, 145 King St West Suite 1400 
* https://www.meetup.com/Toronto-Perl-Mongers/events/rhsxwpyzfblc/

# April

## Polyhack Toronto
* Wednesday, 3rd, 6:30 PM
* H Bar, 859 Queen Street W
* https://www.meetup.com/polyhackTO/events/259646709/

## Unix Unanimous 
* Wednesday, 10th, 6:45 PM
* Ryerson, 245 Church St. [George Vari Engineering Building], ENG201
* http://www.unixunanimous.org/

## Serverless Toronto
* Thursday, 18th, 6:00 PM
* Myplanet, 252 Adelaide St E
* https://www.meetup.com/Serverless-Toronto/events/259565019/

## Achievers Tech Talks
Keeping up with React: What's new in version 16
* Thursday, 25th, 6:00 PM
* 190 Liberty St.
* https://www.meetup.com/achieverstech/events/260063740/

***
If you have an event to add to this monthly list, please contact: scott [at] gtalug [dot] org.
